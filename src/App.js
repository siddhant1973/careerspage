import React from 'react'
import CVForm from './components/cvform/Form'
import Career from './components/career/career'
import './App.scss';

function App() {
  return(
    <div className="App">
      <Career />
    </div>

  );
}

export default App;
