
import './Cvform.scss'
import React, { useState , useRef ,useEffect} from 'react'
import {useForm} from 'react-hook-form'
import { Container, Col, Row, Jumbotron, Form, Button, ButtonGroup, DropdownButton, Dropdown } from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"


let useClickOutside = (handler) => {
  let domNode = useRef();

  useEffect(() => {
    let maybeHandler = (event) => {
      if (domNode.current && !domNode.current.contains(event.target)) {
        console.log(handler)
        handler();
      }
    };

    document.addEventListener("mousedown", maybeHandler);

    return () => {
      document.removeEventListener("mousedown", maybeHandler);
    };
  });

  return domNode;
};

function CVForm() {
  const [values, setValues] = useState([])
  const [selectedFile, setSelectedFile] = useState(null)
  let [isOpen, setIsOpen] = useState(false)
    let domNode = useClickOutside(() => {
        setIsOpen(false);
      });
    
  function onFileChange(e) {
    e.preventDefault();
    setSelectedFile(e.target.files[0])
  }
  const handler=(e)=>{
    console.log(e);
    if(e.target.value===""){
        e.target.className="input-default";
    }else {
    e.target.className="input-success";
}
}
  function printlist() {
    console.log(values)
  }
  function handleChange(e) {
    e.preventDefault()
    const value = e.target.value;
    setValues({
      ...values,
      [e.target.name]: value
    });

  }
  const onFileUpload = (e) => {
    // Create an object of formData 
    const formData = new FormData();

    // Update the formData object 
    formData.append(
      "myFile", values.selectedFile,
    );
  }
  return (
    <div className='wrapper'>
      <Container >
        <Row>
          <Col >
            <Jumbotron fluid className='jumbo_styles'>
              <h1 >{values.name ? `${values.name}'s Form` : "Form"}</h1>
              <p>
                <strong>Fill the form below accurately indicating your potentials and suitability to job applying for.</strong>
              </p>
              <br />
              <Form className='form_style'>

                <Form.Group className='form_group'>
                  <Row className='form_group_label_1'>

                    <Form.Control className={
                      values.name
                        ? "input-group input-success"
                        : "input-group"
                    } placeholder=" Your Name" value={values.name} name="name" onChange={handleChange}></Form.Control>
                  </Row>
                </Form.Group>
                <br />
                <Form.Group as={Col} className='form_group'>
                  <Row className='form_group_label_1'>

                    <Form.Control type="email" className={
                      values.email
                        ? "input-group input-success"
                        : "input-group"
                    } placeholder=" Your Email" value={values.email} name="email" onChange={handleChange} />
                  </Row>
                </Form.Group>
                <br />
                <Form.Group as={Col} className='form_group' >
                  <Row className='form_group_label_1'>

                    <Form.Control
                      type="telephone"
                      placeholder=" Your Phone Number"
                      className={
                        values.phoneno
                          ? "input-group input-success"
                          : "input-group"
                      } value={values.phoneno} name="phoneno" onChange={handleChange} />
                  </Row>
                </Form.Group>
                <br />
                <Form.Group className='form_group'>
                  <Row className='form_group_label_1'>
                    <Form.Control className={
                      values.address
                        ? "input-group input-success"
                        : "input-group"
                    } placeholder=" Your Address" name="address" value={values.address} onChange={handleChange} />
                  </Row>
                </Form.Group>
                <br />
                <Form.Group as={Col} className='form_group'>
                  <Row className='form_group_label_1'>
                    <Form.Control as="select"  className="input-default" name="position" onChange={handler}>
                      <option value="" >Choose position</option>
                      <option >FrontEnd Developer</option>
                      <option >BackEnd Developer</option>
                      <option >Software Tester</option>
                      <option >SEO</option>
                      <option >Web Designer</option>
                    </Form.Control>
                  </Row>
                </Form.Group>
                <br />
                <Form.Group className='form_group'>
                  <Row className='form_group_label_1'>
                    <Form.Label for="files" className={
                      selectedFile
                        ? "input-group input-success"
                        : "input-group"
                    } >
                      {selectedFile ? selectedFile.name : "Upload Resume"}
                    </Form.Label>
                    <input id="files" style={{ display: "none" }} onChange={onFileChange} type="file" />
                  </Row>
                </Form.Group>
                <Form.Group className="form_group_label _1">
                  <Button className="btn btn-styles" placeholder="Submit" onClick={printlist}>Confirm</Button>
                </Form.Group>
              </Form>
            </Jumbotron>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default CVForm
