import pic1 from './data-science.jpg'
import pic2 from './frontend.png'
import pic3 from './data-science2.jpg'
import CvForm from '../cvform/Form'
import React, { useRef, useState, useEffect } from 'react';

import './career.styles.scss';
import { Container, Row, Col } from 'react-bootstrap'

let useClickOutside = (handler) => {
    let domNode = useRef();

    useEffect(() => {
        let maybeHandler = (event) => {
            if (domNode.current && !domNode.current.contains(event.target)) {
                console.log(domNode.current)
                handler();
            }
        };

        document.addEventListener("mousedown", maybeHandler);

        return () => {
            console.log("listener removed")
            document.removeEventListener("mousedown", maybeHandler);
        };
    });

    return domNode;
};

function Career() {
    let [isOpen, setIsOpen] = useState(false)
    let domNode = useClickOutside(() => {
        setIsOpen(false);
    });
    return (
        <Container fluid className="container-component">
            <Row>
                <Col className="welcome-area bg-image">
                    <Row className="header-text">
                        <h1><strong>Solvendo Careers</strong></h1>
                        <p>Solvendo helps you generate this plan for your business with its proprietary algorithm.</p>
                        <a href="#features" className="btn btn-primary">Discover More</a>
                    </Row>
                </Col>
            </Row>
            <Row className="content-area">
                <Row>
                    <Col lg={5} md={12} sm={12} data-scroll-reveal="enter left move 30px over 0.6s after 0.4s" className="jumbo-img">
                        <img src={pic1} alt="" />
                    </Col>
                    <Col lg={1}></Col>
                    <Col lg={5} md={12} sm={12} className="jumbo-text">
                        <Row>
                            <h2 >Let’s discuss about your preferences</h2>
                            <p className="left-text">Nullam sit amet purus libero. Etiam ullamcorper nisl ut augue blandit, at finibus leo efficitur. Nam gravida purus non sapien auctor, ut aliquam magna ullamcorper.</p>
                        </Row>
                    </Col>
                </Row>
            </Row>
            <Row className="content-area">
                <Row>
                    <Col lg={1}></Col>
                    <Col lg={5} md={12} sm={12} className="jumbo-text">
                        <Row>
                            <h2 >Let’s discuss about your preferences</h2>
                            <p className="left-text">Nullam sit amet purus libero. Etiam ullamcorper nisl ut augue blandit, at finibus leo efficitur. Nam gravida purus non sapien auctor, ut aliquam magna ullamcorper.</p>
                        </Row>
                    </Col>
                    <Col lg={5} md={12} sm={12} data-scroll-reveal="enter left move 30px over 0.6s after 0.4s" className="jumbo-img">
                        <img  className='img-pic' src={pic3} alt="" />
                    </Col>
                </Row>
            </Row>
            <Row className='form-area'>
                <Row >
                    <Row className={isOpen ? " overlay-styles" : "overlay-opp"}>
                        <Row ref={domNode}  >
                            <CvForm ref={domNode} className="modal"></CvForm>
                        </Row>
                    </Row>
                </Row>
                <Col lg={4} md={6} sm={12} className='form-item' onClick={() => setIsOpen((isOpen) => !isOpen)}>
                    <Row className="form-text">Frontend Developer</Row>
                    <Row className="form-img"><img src={pic2} alt="" /></Row>
                </Col>
                <Col lg={4} md={6} sm={12} className='form-item' onClick={() => setIsOpen((isOpen) => !isOpen)}>
                    <Row className="form-text">Backend Developer</Row>
                    <Row className="form-img"><img src={pic2} alt="" /></Row>
                </Col>
                <Col lg={4} md={6} sm={12} className='form-item' onClick={() => setIsOpen((isOpen) => !isOpen)}>
                    <Row className="form-text">Data Tester</Row>
                    <Row className="form-img"><img src={pic2} alt="" /></Row>
                </Col>
            </Row>
        </Container>
    );
}

export default Career
